defmodule Lilcommunication.Repo.Migrations.AddColumnsToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :disabled, :boolean, default: false, null: false
      add :admin, :boolean, default: false, null: false
      add :visitor, :boolean, default: false, null: false
      add :verified, :boolean, default: false, null: false
      add :avatar, :string
      add :sig, :string
      add :byline, :string
    end

  end
end
