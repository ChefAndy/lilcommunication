defmodule Lilcommunication.Repo.Migrations.CreateTopics do
  use Ecto.Migration

  def change do
    create table(:topics) do
      add :title, :string
      add :body, :text
      add :acl, :map
      add :sticky, :boolean, default: false, null: false
      add :reacts, :map
      add :labels, :map
      add :user_id, references(:users, on_delete: :nothing, type: :uuid)

      timestamps()
    end

    create index(:topics, [:user_id])
  end
end
