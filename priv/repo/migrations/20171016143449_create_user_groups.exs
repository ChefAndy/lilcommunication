defmodule Lilcommunication.Repo.Migrations.CreateUserGroups do
  use Ecto.Migration

  def change do
    create table(:user_groups) do
      add :name, :string
      add :allow, :map
      add :deny, :map

      timestamps()
    end

  end
end
