defmodule Lilcommunication.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :title, :string
      add :body, :text
      add :acl, :map
      add :hidden, :text
      add :sticky, :boolean, default: false, null: false
      add :reacts, :map
      add :user_id, references(:users, on_delete: :nothing, type: :uuid)

      timestamps()
    end

    create index(:posts, [:user_id])
  end
end
