defmodule Lilcommunication.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table(:threads) do
      add :title, :string
      add :body, :text
      add :acl, :map
      add :sticky, :boolean, default: false, null: false
      add :reacts, :map
      add :views, :integer
      add :user_id, references(:users, on_delete: :nothing, type: :uuid)

      timestamps()
    end

    create index(:threads, [:user_id])
  end
end
