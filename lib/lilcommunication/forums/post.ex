defmodule Lilcommunication.Forums.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias Lilcommunication.Forums.Post


  schema "posts" do
    field :acl, :map
    field :body, :string
    field :hidden, :string
    field :reacts, :map
    field :sticky, :boolean, default: false
    field :title, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Post{} = post, attrs) do
    post
    |> cast(attrs, [:title, :body, :acl, :hidden, :sticky, :reacts])
    |> validate_required([:title, :body, :acl, :hidden, :sticky, :reacts])
  end
end
