defmodule Lilcommunication.Forums.Thread do
  use Ecto.Schema
  import Ecto.Changeset
  alias Lilcommunication.Forums.Thread


  schema "threads" do
    field :acl, :map
    field :body, :string
    field :reacts, :map
    field :sticky, :boolean, default: false
    field :title, :string
    field :views, :integer
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Thread{} = thread, attrs) do
    thread
    |> cast(attrs, [:title, :body, :acl, :sticky, :reacts, :views, :sticky])
    |> validate_required([:title, :body, :acl, :sticky, :reacts, :views, :sticky])
  end
end
