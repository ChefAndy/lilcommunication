defmodule Lilcommunication.Forums.Topic do
  use Ecto.Schema
  import Ecto.Changeset
  alias Lilcommunication.Forums.Topic


  schema "topics" do
    field :acl, :map
    field :body, :string
    field :labels, :map
    field :reacts, :map
    field :sticky, :boolean, default: false
    field :title, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(%Topic{} = topic, attrs) do
    topic
    |> cast(attrs, [:title, :body, :acl, :sticky, :reacts, :labels])
    |> validate_required([:title, :body, :acl, :sticky, :reacts, :labels])
  end
end
