defmodule Lilcommunication.Accounts.UserGroups do
  use Ecto.Schema
  import Ecto.Changeset
  alias Lilcommunication.Accounts.UserGroups


  schema "user_groups" do
    field :allow, :map
    field :deny, :map
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(%UserGroups{} = user_groups, attrs) do
    user_groups
    |> cast(attrs, [:name, :allow, :deny])
    |> validate_required([:name, :allow, :deny])
  end
end
