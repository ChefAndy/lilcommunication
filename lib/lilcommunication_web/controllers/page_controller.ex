defmodule LilcommunicationWeb.PageController do
  use LilcommunicationWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
