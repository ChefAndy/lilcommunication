# Lilcommunication
This is a pre-alpha communication tool for Harvard's Library Innovation Lab. It's essentially a forum with a real-time chat, and intant messaging. It's primarily an educational project to teach me phoenix 1.3, but it's also an exploration in what more traditional forum software can do for workplace communication that traditional email and glorified IRC clients can't. I think the layer of organization of a forum for more official conversations, while still providing the fun of real time chat for more laid back conversations, will make this communication tool  uniquely useful for a work team.

#### Visit LIL's Website
 http://lil.law.harvard.edu
