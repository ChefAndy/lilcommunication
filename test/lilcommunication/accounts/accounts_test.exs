defmodule Lilcommunication.AccountsTest do
  use Lilcommunication.DataCase

  alias Lilcommunication.Accounts

  describe "user_groups" do
    alias Lilcommunication.Accounts.UserGroups

    @valid_attrs %{allow: %{}, deny: %{}, name: "some name"}
    @update_attrs %{allow: %{}, deny: %{}, name: "some updated name"}
    @invalid_attrs %{allow: nil, deny: nil, name: nil}

    def user_groups_fixture(attrs \\ %{}) do
      {:ok, user_groups} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user_groups()

      user_groups
    end

    test "list_user_groups/0 returns all user_groups" do
      user_groups = user_groups_fixture()
      assert Accounts.list_user_groups() == [user_groups]
    end

    test "get_user_groups!/1 returns the user_groups with given id" do
      user_groups = user_groups_fixture()
      assert Accounts.get_user_groups!(user_groups.id) == user_groups
    end

    test "create_user_groups/1 with valid data creates a user_groups" do
      assert {:ok, %UserGroups{} = user_groups} = Accounts.create_user_groups(@valid_attrs)
      assert user_groups.allow == %{}
      assert user_groups.deny == %{}
      assert user_groups.name == "some name"
    end

    test "create_user_groups/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user_groups(@invalid_attrs)
    end

    test "update_user_groups/2 with valid data updates the user_groups" do
      user_groups = user_groups_fixture()
      assert {:ok, user_groups} = Accounts.update_user_groups(user_groups, @update_attrs)
      assert %UserGroups{} = user_groups
      assert user_groups.allow == %{}
      assert user_groups.deny == %{}
      assert user_groups.name == "some updated name"
    end

    test "update_user_groups/2 with invalid data returns error changeset" do
      user_groups = user_groups_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user_groups(user_groups, @invalid_attrs)
      assert user_groups == Accounts.get_user_groups!(user_groups.id)
    end

    test "delete_user_groups/1 deletes the user_groups" do
      user_groups = user_groups_fixture()
      assert {:ok, %UserGroups{}} = Accounts.delete_user_groups(user_groups)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user_groups!(user_groups.id) end
    end

    test "change_user_groups/1 returns a user_groups changeset" do
      user_groups = user_groups_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user_groups(user_groups)
    end
  end

  describe "users" do
    alias Lilcommunication.Accounts.User

    @valid_attrs %{email: "some email", password_hash: "some password_hash"}
    @update_attrs %{email: "some updated email", password_hash: "some updated password_hash"}
    @invalid_attrs %{email: nil, password_hash: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.password_hash == "some password_hash"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.email == "some updated email"
      assert user.password_hash == "some updated password_hash"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
