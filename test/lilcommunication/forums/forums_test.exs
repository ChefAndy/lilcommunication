defmodule Lilcommunication.ForumsTest do
  use Lilcommunication.DataCase

  alias Lilcommunication.Forums

  describe "posts" do
    alias Lilcommunication.Forums.Post

    @valid_attrs %{acl: %{}, body: "some body", hidden: "some hidden", reacts: %{}, sticky: true, title: "some title"}
    @update_attrs %{acl: %{}, body: "some updated body", hidden: "some updated hidden", reacts: %{}, sticky: false, title: "some updated title"}
    @invalid_attrs %{acl: nil, body: nil, hidden: nil, reacts: nil, sticky: nil, title: nil}

    def post_fixture(attrs \\ %{}) do
      {:ok, post} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Forums.create_post()

      post
    end

    test "list_posts/0 returns all posts" do
      post = post_fixture()
      assert Forums.list_posts() == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = post_fixture()
      assert Forums.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      assert {:ok, %Post{} = post} = Forums.create_post(@valid_attrs)
      assert post.acl == %{}
      assert post.body == "some body"
      assert post.hidden == "some hidden"
      assert post.reacts == %{}
      assert post.sticky == true
      assert post.title == "some title"
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Forums.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = post_fixture()
      assert {:ok, post} = Forums.update_post(post, @update_attrs)
      assert %Post{} = post
      assert post.acl == %{}
      assert post.body == "some updated body"
      assert post.hidden == "some updated hidden"
      assert post.reacts == %{}
      assert post.sticky == false
      assert post.title == "some updated title"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = post_fixture()
      assert {:error, %Ecto.Changeset{}} = Forums.update_post(post, @invalid_attrs)
      assert post == Forums.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = post_fixture()
      assert {:ok, %Post{}} = Forums.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Forums.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = post_fixture()
      assert %Ecto.Changeset{} = Forums.change_post(post)
    end
  end

  describe "topics" do
    alias Lilcommunication.Forums.Topic

    @valid_attrs %{acl: %{}, body: "some body", labels: %{}, reacts: %{}, sticky: true, title: "some title"}
    @update_attrs %{acl: %{}, body: "some updated body", labels: %{}, reacts: %{}, sticky: false, title: "some updated title"}
    @invalid_attrs %{acl: nil, body: nil, labels: nil, reacts: nil, sticky: nil, title: nil}

    def topic_fixture(attrs \\ %{}) do
      {:ok, topic} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Forums.create_topic()

      topic
    end

    test "list_topics/0 returns all topics" do
      topic = topic_fixture()
      assert Forums.list_topics() == [topic]
    end

    test "get_topic!/1 returns the topic with given id" do
      topic = topic_fixture()
      assert Forums.get_topic!(topic.id) == topic
    end

    test "create_topic/1 with valid data creates a topic" do
      assert {:ok, %Topic{} = topic} = Forums.create_topic(@valid_attrs)
      assert topic.acl == %{}
      assert topic.body == "some body"
      assert topic.labels == %{}
      assert topic.reacts == %{}
      assert topic.sticky == true
      assert topic.title == "some title"
    end

    test "create_topic/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Forums.create_topic(@invalid_attrs)
    end

    test "update_topic/2 with valid data updates the topic" do
      topic = topic_fixture()
      assert {:ok, topic} = Forums.update_topic(topic, @update_attrs)
      assert %Topic{} = topic
      assert topic.acl == %{}
      assert topic.body == "some updated body"
      assert topic.labels == %{}
      assert topic.reacts == %{}
      assert topic.sticky == false
      assert topic.title == "some updated title"
    end

    test "update_topic/2 with invalid data returns error changeset" do
      topic = topic_fixture()
      assert {:error, %Ecto.Changeset{}} = Forums.update_topic(topic, @invalid_attrs)
      assert topic == Forums.get_topic!(topic.id)
    end

    test "delete_topic/1 deletes the topic" do
      topic = topic_fixture()
      assert {:ok, %Topic{}} = Forums.delete_topic(topic)
      assert_raise Ecto.NoResultsError, fn -> Forums.get_topic!(topic.id) end
    end

    test "change_topic/1 returns a topic changeset" do
      topic = topic_fixture()
      assert %Ecto.Changeset{} = Forums.change_topic(topic)
    end
  end

  describe "threads" do
    alias Lilcommunication.Forums.Thread

    @valid_attrs %{acl: %{}, body: "some body", reacts: %{}, sticky: true, title: "some title", views: 42}
    @update_attrs %{acl: %{}, body: "some updated body", reacts: %{}, sticky: false, title: "some updated title", views: 43}
    @invalid_attrs %{acl: nil, body: nil, reacts: nil, sticky: nil, title: nil, views: nil}

    def thread_fixture(attrs \\ %{}) do
      {:ok, thread} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Forums.create_thread()

      thread
    end

    test "list_threads/0 returns all threads" do
      thread = thread_fixture()
      assert Forums.list_threads() == [thread]
    end

    test "get_thread!/1 returns the thread with given id" do
      thread = thread_fixture()
      assert Forums.get_thread!(thread.id) == thread
    end

    test "create_thread/1 with valid data creates a thread" do
      assert {:ok, %Thread{} = thread} = Forums.create_thread(@valid_attrs)
      assert thread.acl == %{}
      assert thread.body == "some body"
      assert thread.reacts == %{}
      assert thread.sticky == true
      assert thread.title == "some title"
      assert thread.views == 42
    end

    test "create_thread/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Forums.create_thread(@invalid_attrs)
    end

    test "update_thread/2 with valid data updates the thread" do
      thread = thread_fixture()
      assert {:ok, thread} = Forums.update_thread(thread, @update_attrs)
      assert %Thread{} = thread
      assert thread.acl == %{}
      assert thread.body == "some updated body"
      assert thread.reacts == %{}
      assert thread.sticky == false
      assert thread.title == "some updated title"
      assert thread.views == 43
    end

    test "update_thread/2 with invalid data returns error changeset" do
      thread = thread_fixture()
      assert {:error, %Ecto.Changeset{}} = Forums.update_thread(thread, @invalid_attrs)
      assert thread == Forums.get_thread!(thread.id)
    end

    test "delete_thread/1 deletes the thread" do
      thread = thread_fixture()
      assert {:ok, %Thread{}} = Forums.delete_thread(thread)
      assert_raise Ecto.NoResultsError, fn -> Forums.get_thread!(thread.id) end
    end

    test "change_thread/1 returns a thread changeset" do
      thread = thread_fixture()
      assert %Ecto.Changeset{} = Forums.change_thread(thread)
    end
  end
end
